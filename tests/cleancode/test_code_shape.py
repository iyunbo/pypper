import pathlib
import sys
import os
from os.path import isfile

import pytest

from pypper.cleancode.code_shape import FileShape, FunctionShape
from pypper.cleancode.size import FileSize
from pypper.cleancode.stat import Stat


def test_fileview_creation():
    fileview = FileShape(sample())


def test_valid_file_path():
    fileview = FileShape(sample())
    assert "Sample.java" in fileview.file_path
    assert isfile(fileview.file_path)


def test_get_file_size():
    fileview = FileShape(sample())
    assert fileview.size


def test_get_size_in_bytes():
    fileview = FileShape(sample())
    if os.name == 'nt':
        assert fileview.size.size == 435
    else:
        assert fileview.size.size == 409


def sample(filename="Sample.java"):
    dir_path = pathlib.Path(__file__).absolute()
    sample_file = str(dir_path.with_name(filename))
    return sample_file


def test_file_size_human_readable_display():
    filesize = FileSize(999)
    assert str(filesize) == "999.00 Bytes"


def test_file_size_for_1_char():
    filesize = FileSize(1)
    assert str(filesize) == "1 Byte"


def test_file_size_over_1k():
    filesize = FileSize(2048)
    assert str(filesize) == "2.00 KB"


def test_file_size_0():
    filesize = FileSize(0)
    assert str(filesize) == "0 Byte"


def test_file_size_tb():
    filesize = FileSize(1458000256012)
    assert str(filesize) == "1.33 TB"


def test_file_too_big_size():
    with pytest.raises(ValueError) as error:
        assert str(FileSize(sys.maxsize ** 10))
    assert caused_by_out_of_bounds(error)


def caused_by_out_of_bounds(error):
    return "file size out of bounds" in str(error.value)


def test_file_negative_size():
    with pytest.raises(ValueError) as error:
        assert str(FileSize(-1))
    caused_by_out_of_bounds(error)


def test_file_size_does_not_equal_to_other_type():
    assert FileSize(1) != 1


def test_file_line_stat():
    fileview = FileShape(sample())
    line_stat = fileview.line_stat
    assert line_stat.min == 2
    assert line_stat.max == 52


def test_line_stat():
    stat = Stat([1, 2, 3, 4, 5, 6, 7])
    assert stat.max == 7
    assert stat.min == 1
    assert stat.median == 4
    assert stat.average == 4


def test_function_shape_creation():
    function_shape = FunctionShape(sample())
    assert function_shape.functions == [("compareTo", 3, False), ("test", 1, True)]
    assert function_shape.shape.max == 3
    assert function_shape.shape.min == 1
    assert function_shape.documentation_ratio == 0.5
