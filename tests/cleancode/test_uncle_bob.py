from pypper.cleancode.uncle_bob import MAX_FILE_LINES, EXPECTED_AVERAGE_FILE_LINES


def test_file_lines_rule():
    assert MAX_FILE_LINES > EXPECTED_AVERAGE_FILE_LINES
