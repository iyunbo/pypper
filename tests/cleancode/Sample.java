package cleancode;

/**
 * this is a documentation
 */
public class Sample implements Comparable<String> {
	@Override
	public int compareTo(String o) {
		System.out.println("start");
		test();
		// this is a comment
		return this.compareTo(o);
	}

	/**
	 * this doc is for method
	 */
	private void test() {
		/*
		 * multiple lines comment
		 * more
		 * and more
		 */
		System.out.println("testing");
	}
}
