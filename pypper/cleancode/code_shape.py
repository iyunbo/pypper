import javalang
from pathlib import Path

from .size import FileSize
from .stat import Stat


class FileShape:
    def __init__(self, file_path: str):
        self.file_path = file_path
        stat = Path(file_path).stat()
        self.size = FileSize(stat.st_size)
        self.line_stat = line_stat(file_path)
        self.function_shape = FunctionShape(file_path)


class FunctionShape:
    def __init__(self, file_path: str):
        with open(file_path) as f:
            file_content = f.read()
            self.functions = get_functions(file_content)
            functions_size = [function[1] for function in self.functions]
            have_documentation = [function[2] for function in self.functions]
            self.shape = Stat(functions_size)
            self.documentation_ratio = sum(have_documentation) / len(have_documentation)


def count_lines(f):
    lines = []
    empty_indexes = []

    for index, line in enumerate(f):
        if not line.strip():
            empty_indexes.append(index)
        else:
            lines.append(len(line))

    return lines, empty_indexes


def get_functions(file_content):
    tree = javalang.parse.parse(file_content)
    shape = []
    for _, node in tree.filter(javalang.tree.ClassDeclaration):
        for method in node.methods:
            shape.append((method.name, len(method.body), method.documentation is not None))
    return shape


def line_stat(file_path):
    with open(file_path) as f:
        lines, _ = count_lines(f)
    return Stat(lines)
