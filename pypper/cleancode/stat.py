import numpy as np


class Stat:
    def __init__(self, sample: list):
        self.min = min(sample)
        self.max = max(sample)
        array = np.array(sample)
        self.median = np.percentile(array, 50)
        self.average = np.average(array)
